<h2 class="elzevir">Все песни...</h2>
<div class="audio2_html5">
    <audio id="audio2_html5_black" preload="metadata">
        <div class="xaudioplaylist">
            <?php
            foreach (db::$songs as $s) {
                $_ = '<ul>';
                $_ .= '<li class="xtitle">' . $s['name'] . '</li>';
                $_ .= '<li class="xauthor">Стихи: ' . db::$authors[$s['author']] . '</li>';
                $_ .= '<li class="xcategory">Все песни;</li>';
                $_ .= '<li class="xsources_mp3">' . $s['file'] . '</li>';
                $_ .= '</ul>';

                print($_);
            }
            ?>
        </div>
        Браузер устарел и не поддерживает возможности для воспроизведения песен.
        Пожалуйста, используйте <a href="https://google.com/intl/ru/chrome/browser/">Chrome Browser!</a>
    </audio>
</div>

<script>
    viewHelper.setAudioPlayerList('audio2_html5_black');
</script>
