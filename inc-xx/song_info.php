<?php

$song = db::$songs[view::$id - 1];
?>
<h2 class="elzevir"><?= $song['name'] ?></h2>
<h4> Стихи: <?= db::$authors[$song['author']] ?></h4>
<?php if ((int)$song['arrangement'] > 0) { ?>
    <h4> Аранжировка: <?= db::$arrangements[$song['arrangement']] ?></h4>
<?php } ?>
<h4> Скачать: <a href="/download.php?the_file=<?= $song['file'] ?>">ссылка</a></h4>
<div class="audio1_html5">
    <audio id="audio1_html5_white" preload="metadata">
        <div class="xaudioplaylist">
            <ul>
                <li class="xtitle"><?= $song['name'] ?> <span
                        class="stihi-in-title">(стихи: <?= db::$authors[$song['author']] ?>)</span></li>
                <li class="xauthor">Стихи: <?= db::$authors[$song['author']] ?></li>
                <li class="xcategory">Все песни;</li>
                <li class="xsources_mp3"><?= $song['file'] ?></li>
            </ul>
        </div>
        Браузер устарел и не поддерживает возможности для воспроизведения песен.
        Пожалуйста, используйте <a href="https://google.com/intl/ru/chrome/browser/">Chrome Browser!</a>
    </audio>
</div>
<div id="stihi">
    <?php
    $stih = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/stihi/' . $song['id'] . '.txt');
    $stih = nl2br($stih);
    print ($stih);

    ?>
</div>
<script>
    viewHelper.setMiniAudioPlayerList("audio1_html5_white");
</script>


