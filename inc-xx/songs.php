<h2 class="elzevir">Песни...</h2>
<div class="grid-x grid-padding-x small-up-2 medium-up-3">
    <?php
    foreach (db::$songs as $s) {
        $_ = '<div class="cell">';
        $_ .= '<div class="card">';
        $_ .= '<div class="card-section">';

        $_ .= '<a href="/песни/' . $s['id'] . '"><h4>' . $s['name'] . '</h4></a>';
        $_ .= '<p>Стихи: ' . db::$authors[$s['author']] . '</p>';

        $_ .= '</div>';
        $_ .= '</div>';
        $_ .= '</div>';

        print($_);
    }
    ?>
</div>
