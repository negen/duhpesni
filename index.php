<?php
$do = $_REQUEST['do'];
$id = (int)$_REQUEST['id'];

class view
{
    public static $do = '';
    public static $id = 0;
    public static $hash = '';
    public static $add_js = '';
    public static $add_css = '';
    public static $body_file = '';
    public static $page_title = 'Духовные песни игумена Иосифа';

    public static function init()
    {
        self::$do = $_REQUEST['do'];
        self::$hash = md5(date('Ymd'));

        switch (self::$do) {
            case "download":
                $file = htmlspecialchars(urldecode($_GET['the_file']));
                $parse_url = parse_url($file);
                $path_info = pathinfo($parse_url['path']);
                if ($path_info['extension'] == 'mp3' && $path_info['dirname'] == '/media') {
                    header('X-Accel-Redirect: ' . $file);
                    header('Content-Type: audio/mpeg');
                    header('Content-Disposition: attachment; filename="' . basename($file) . '"');
                }
                exit();

            case "test":
                self::$body_file = "test.php";
                break;
            case "":
                self::$body_file = "biograf.php";
                break;

            case "listen_all_songs":
                include_once('inc-xx/db.php');
                self::$page_title = "Слушать духовные песни игумена Иосифа";
                self::$body_file = "listen_all_songs.php";
                self::$add_js = '    
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <script src="/js/vendor/audio-player-pro/jquery.mousewheel.min.js"></script>
    <script src="/js/vendor/audio-player-pro/jquery.touchSwipe.min.js"></script>
    <script src="/js/vendor/audio-player-pro/audio2_html5.js"></script>
    <script src="/js/viewHelper.js"></script>';
                self::$add_css = '<link rel="stylesheet" href="/css/audio-player-pro/audio2_html5.css">';
                break;

            case "songs":
                include_once('inc-xx/db.php');
                self::$page_title = "Все песни игумена Иосифа";
                self::$body_file = "songs.php";
                break;

            case "song_info":
                include_once('inc-xx/db.php');
                self::$id = (int)$_REQUEST['id'];
                if (self::$id <= 0 || empty(db::$songs[self::$id - 1])) {
                    self::$body_file = "no-info.php";
                    break;
                }

                $song = db::$songs[view::$id - 1];
                self::$page_title = $song['name'] . ', стихи: ' . db::$authors[$song['author']];
                self::$body_file = "song_info.php";
                self::$add_js = '    
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>
    <script src="/js/vendor/audio-player-pro/jquery.mousewheel.min.js"></script>
    <script src="/js/vendor/audio-player-pro/jquery.touchSwipe.min.js"></script>
    <script src="/js/vendor/audio-player-pro/audio1_html5.js"></script>
    <script src="/js/viewHelper.js"></script>';
                self::$add_css = '<link rel="stylesheet" href="/css/audio-player-pro/audio1_html5.css">';

                break;
            default:
                self::$body_file = "no-info.php";
        }
    }
}

// инициализация
view::init();
?><!doctype html>
<html class="no-js" lang="ru" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-W9WZRRJ');</script>
    <!-- End Google Tag Manager -->

    <title><?= view::$page_title ?></title>
    <link rel="stylesheet" href="/css/foundation.min.css">
    <?= view::$add_css ?>
    <link rel="stylesheet" href="/css/app.css?d=<?= view::$hash ?>">

    <script src="/js/vendor/jquery.js"></script>
    <script src="/js/vendor/foundation.min.js"></script>
    <?= view::$add_js ?>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W9WZRRJ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="top-bar" id="responsive-menu">
    <div class="top-bar-left">
        <ul class="dropdown menu" data-dropdown-menu>
            <li><a class="f-color-x" href="/">Автобиография</a></li>
            <li><a class="f-color-x" href="/песни">Песни</a></li>
            <li><a class="f-color-x" href="/слушать-все-песни">Слушать</a></li>
        </ul>
    </div>
</div>

<div class="grid-container">
    <div class="grid-x grid-padding-x">
        <div class="large-12 cell">
            <h1 class="center elzevir">Духовные песни игумена Иосифа</h1>
        </div>
    </div>

    <div class="grid-x grid-padding-x">
        <div class="large-12 cell" id="x-main">
            <?php include_once("inc-xx/" . view::$body_file) ?>
        </div>
    </div>
    <div class="grid-x grid-padding-x">
        <footer>
            <img src="/image/footer.png" alt>
        </footer>
    </div>
</div>

<script src="/js/vendor/stickyFooter.js"></script>
</body>
</html>
