var viewHelper = (function() {

    var _setAudioPlayerList = function(elemId) {
        $('#' + elemId).audio2_html5({
            skin: 'blackControllers',
            autoPlay:false,
            initialVolume:0.5,
            showRewindBut:true,
            showShuffleBut:true,
            showDownloadBut:true,
            showFacebookBut:false,
            showTwitterBut:false,
            showBuyBut: false,
            showLyricsBut: false,
            showAuthor: true,
            responsive:true,
            shuffle:false,

            loop: true,
            firstCateg: 'Все песни',
            numberOfThumbsPerScreen: 15,

            playerBg: '#feebbb',
            bufferEmptyColor: '#737373',
            bufferFullColor: '#bababa',
            seekbarColor: '#000000',
            volumeOffColor: '#bababa',
            volumeOnColor: '#000000',
            timerColor: '#000000',
            songTitleColor: '#000000',
            songAuthorColor: '#888888',
            bordersDivColor: '#cccccc',
            playlistTopPos:0,
            playlistBgColor:'#feebbb',
            playlistRecordBgOffColor:'#feebbb',
            playlistRecordBgOnColor:'#feebbb',
            playlistRecordBottomBorderOffColor:'#cccccc',
            playlistRecordBottomBorderOnColor:'#8d8d8d',
            playlistRecordTextOffColor:'#777777',
            playlistRecordTextOnColor:'#000000',

            categoryRecordBgOffColor:'#fdd776',
            categoryRecordBgOnColor:'#fdd776',
            categoryRecordBottomBorderOffColor:'#2f2f2f',
            categoryRecordBottomBorderOnColor:'#2f2f2f',
            categoryRecordTextOffColor:'#4c4c4c',
            categoryRecordTextOnColor:'#f90000',

            selectedCategBg: '#c7c7c7',
            selectedCategOffColor: '#333333',
            selectedCategOnColor: '#f90000',
            selectedCategMarginBottom:12,

            searchAreaBg: '#c7c7c7',
            searchInputBg:'#feebbb',
            searchInputBorderColor:'#c7c7c7',
            searchInputTextColor:'#333333',
            searchInputText: 'Найти'
        });
    };

    var _setMiniAudioPlayerList = function(elemId) {
        $('#' + elemId).audio1_html5({
            skin: 'blackControllers',

            playerPadding:5,
            autoPlay:true,
            showRewindBut:true,
            showPreviousBut:false,
            showNextBut:false,
            showPlaylistBut:false,
            showVolumeBut:true,
            showVolumeSliderBut:true,
            showSeekBar:false,
            showAuthor:false,
            showTitle:true,
            showPlaylist:false,

            playerBg: '#FFFFFF',
            bufferEmptyColor: '#d5d5d5',
            bufferFullColor: '#afafaf',
            seekbarColor: '#000000',
            volumeOffColor: '#afafaf',
            volumeOnColor: '#000000',
            timerColor: '#000000',
            songAuthorTitleColor: '#000000',

            playlistBgColor:'#ffffff',
            playlistRecordBgOffColor:'#ffffff',
            playlistRecordBgOnColor:'#c9c9c9',
            playlistRecordBottomBorderOffColor:'#838383',
            playlistRecordBottomBorderOnColor:'#000000',
            playlistRecordTextOffColor:'#838383',
            playlistRecordTextOnColor:'#000000'
         });
     };

    return {
        setAudioPlayerList: _setAudioPlayerList,
        setMiniAudioPlayerList: _setMiniAudioPlayerList
    }
})();
