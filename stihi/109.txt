Мы друг друга очень часто не прощаем…
Даже больше… Не стараемся простить…
Мы свои грехи почти не замечаем,
Нам важней других в ошибках уличить…

Не прощаем, потому что сердцу больно…
Только боли мы не чувствуем чужой…
И черствеем безвозвратно и невольно…
И становится холодным мир большой.

Мы от злости забываем часто Бога…
Он прощал, оставив место доброте…
Злость в душе рождает вечную тревогу…
От обиды в сердце места нет мечте.

А рискните отпустить свои обиды
И поймите, жизнь других – не ваша жизнь!
Слишком хрупкие сердца под внешним видом…
В поединке сердца с разумом сошлись…

А давайте не считать ошибки брата...
И друзей своих ошибки не считать…
Эгоизмом радость прежняя измята… 
Перемены нужно с сердца начинать…

Мы друг друга очень часто не прощали…
Если так, тогда и нас нельзя простить!
Если б боль друг друга чаще замечали,
То не только бы себя могли любить…