В море лебеди. Лебеди плавают.
Полдень, ветер и синь без конца,
Всей своей красотой величавою
Нам являют всеблагость Творца.

Ах вы, лебеди, птицы небесные,
Под лазурной небес высотой
Наши будни мирские и тесные
Озарили вы нам красотой.

Вот мы молимся душами падшими
Здесь вблизи ваших радостных стай,
Чтобы взмыть нам надеждами нашими
Белокрылою верой в Христа.

Как под вашими крыльями белыми
Остаются внизу города,
Так и прихоти наши дебелые
Нам забыть бы, забыть навсегда,

Чтоб не к жадности утлой, а к благости
Лебедино взмывали сердца,
Чтоб искать нам духовные радости
Только в истинной вере в Христа.

Ах, как лебеди в море купаются,
Машут крыльями, стелются ниц,
И Господь с высоты улыбается
Красоте этих царственных птиц.

Ах вы, лебеди, птицы небесные,
Под лазурной небес высотой
Наши будни мирские и тесные
Озарили вы нам красотой.
